package kr.toandto.stopwatch

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.concurrent.timer

class MainActivity : AppCompatActivity() {

    private var time = 0
    private var isRunning = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //랩 타입 버튼에 이벤트 연결
        lapButton.setOnClickListener {
            recordLaptime()
        }


        fab.setOnClickListener {
            isRunning = !isRunning

            if (isRunning) {
                start()
            } else {
                pause()
            }
        }
        val textView = TextView(this)
        textView.text = ""
        lapLayout.addView(textView)

        //초기화버튼에 이벤트연결
        resetFab.setOnClickListener {
            reset()
        }
    }


    //랩 타임 기록 메서드
    private var lap = 1
    private fun recordLaptime(){
        val lapTime = this.time
        val textView = TextView(this)
        textView.text = "$lap LAP : ${lapTime / 100}.${lapTime % 100}"

        lapLayout.addView(textView, 0)
        lap++
    }

    private var timerTask: Timer? = null

    //타이머 시작
    private fun start() {
        fab.setImageResource(R.drawable.ic_pause_black_24dp)

        timerTask = timer(period = 10) {
            time++
            val sec = time / 100
            val milli = time % 100
            runOnUiThread {
                secTextVi.text = "$sec"
                milliTextVi.text = "$milli"

            }
        }
    }
    //타이머 일시정지
    private fun pause() {
        fab.setImageResource(R.drawable.ic_play_arrow_black_24dp)
        timerTask?.cancel()
    }
    //타이머 초기화
    private fun reset() {
        timerTask?.cancel()

        //모든 변수 초기화
        time = 0
        isRunning = false
        fab.setImageResource(R.drawable.ic_play_arrow_black_24dp)
        secTextVi.text = "0"
        milliTextVi.text = "00"


        //모든 랩타임 제거
        lapLayout.removeAllViews()
        lap = 1
    }
}

